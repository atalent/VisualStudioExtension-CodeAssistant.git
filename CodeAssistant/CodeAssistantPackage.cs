﻿using CodeAssistant.Commands.CSharp;
using CodeAssistant.Commands.Global;
using CodeAssistant.Commands.IDE;
using CodeAssistant.Commands.Unity;
using CodeAssistant.Commands.Wpf;
using CommonUtils;
using Microsoft;
using Microsoft.VisualStudio.Shell;
using System;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;
using System.Threading;
using Task = System.Threading.Tasks.Task;

namespace CodeAssistant
{
    [PackageRegistration(UseManagedResourcesOnly = true, AllowsBackgroundLoading = true)]
    [Guid("efad153b-8c86-4123-a542-f5a87e91e2c4")]
    [ProvideMenuResource("Menus.ctmenu", 1)]
    public sealed class CodeAssistantPackage : AsyncPackage
    {
        static readonly Guid _menuGroupId = new Guid("e58bdf9b-fa80-4ec8-ac16-0908da7cdbc5");
        static OleMenuCommandService _commandService;

        private static void AddMenuCommand(int id, EventHandler handler)
        => _commandService.AddCommand(new MenuCommand(handler, new CommandID(_menuGroupId, id)));

        protected override async Task InitializeAsync(CancellationToken cancellationToken, IProgress<ServiceProgressData> progress)
        {
            await JoinableTaskFactory.SwitchToMainThreadAsync(cancellationToken);
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(DisposalToken);

            _commandService = await GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Assumes.Present(_commandService);

            EditorUtil.SetServiceProvider(this);

            //显示顺序是按照ID来的，跟添加顺序和ASCII顺序无关，ID随便写，但是要与vsct中统一

            //CSharp
            AddMenuCommand(1001, OrderFunctions.Handler);
            AddMenuCommand(1002, LambdaFunctions.Handler);
            AddMenuCommand(1003, AlignLambdas.Handler);
            AddMenuCommand(1004, MultiplyOperate.Handler);
            AddMenuCommand(1005, SwitchLeftRight.Handler);

            //WPF
            AddMenuCommand(2001, CenterTopXmal.Handler);
            AddMenuCommand(2002, CenterMiddleXmal.Handler);
            AddMenuCommand(2003, SortGridElements.Handler);

            //CSharp-Unity
            AddMenuCommand(3001, AddEditorDefinition.Handler);
            AddMenuCommand(3002, InsertEditorDefinition.Handler);
            AddMenuCommand(3003, RemoveEditorDefinition.Handler);
            AddMenuCommand(3004, NewFile.Handler);
            AddMenuCommand(3005, NewFolder.Handler);

            //IDE
            AddMenuCommand(5001, SameFolderNames.Handler);
            AddMenuCommand(5002, GetGuidLowerNoBar.Handler);
            AddMenuCommand(5003, GetGuidLowerWithBars.Handler);
            AddMenuCommand(5004, GetGuidUpperNoBar.Handler);
            AddMenuCommand(5005, GetGuidUpperWithBars.Handler);

            //Global
            AddMenuCommand(9001, Home.Handler);
        }
    }
}

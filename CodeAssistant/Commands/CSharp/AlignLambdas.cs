﻿using CommonUtils;
using CommonUtils.CodeFactory;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;

namespace CodeAssistant.Commands.CSharp
{
    class AlignLambdas
    {
        public static void Handler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            var document = dte.ActiveDocument;

            if (document == null || FileUtil.GetExtension(document.FullName) != ".cs")
            {
                EditorUtil.ShowMessage("Please Open and Select a CSharp File");
                return;
            }

            FunctionUtil.AlignLambdas(document.FullName);
            EditorUtil.ShowMessage(document.Name + " has formatted !");
        }
    }
}

﻿using CommonUtils;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;
using System.Text;

namespace CodeAssistant.Commands.CSharp
{
    class SwitchLeftRight
    {
        public static void Handler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var text = EditorUtil.GetSelectedText();
            var newText = new StringBuilder();
            var lines = text.GetLines();
            for (int index = 0; index < lines.Length; index++)
            {
                var line = lines[index];

                if (index != 0)
                    newText.AppendLine();

                var cells = line.Split('=');
                if (cells.Length == 2)
                {
                    newText.Append(cells[1].RemoveChar(';'));
                    newText.Append('=');
                    newText.Append(cells[0] + ';');
                }
                else
                {
                    newText.Append(line);
                }
            }
            EditorUtil.ReplaceSelection(newText.ToString());
        }
    }
}

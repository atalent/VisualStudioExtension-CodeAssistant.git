﻿using CommonUtils;
using Microsoft.VisualStudio.Shell;
using System;

namespace CodeAssistant.Commands.Global
{
    class Home
    {
        public static void Handler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            ProcessUtil.OpenUrl("https://marketplace.visualstudio.com/items?itemName=atalent.CodeAssistant");
        }
    }
}

﻿using CommonUtils;
using System;
using System.Windows.Forms;

namespace CodeAssistant.Commands.IDE
{
    class GetGuidLowerNoBar
    {
        public static void Handler(object sender, EventArgs e)
        {
            var guid = StringUtil.GetGuidLowerNoBar();
            Clipboard.SetText(guid);
            MessageUtil.ShowInput("已复制到剪切板", guid);
        }
    }
}

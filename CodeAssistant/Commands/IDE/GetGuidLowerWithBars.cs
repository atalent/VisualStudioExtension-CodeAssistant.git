﻿using CommonUtils;
using System;
using System.Windows.Forms;

namespace CodeAssistant.Commands.IDE
{
    class GetGuidLowerWithBars
    {
        public static void Handler(object sender, EventArgs e)
        {
            var guid = StringUtil.GetGuidLowerWithBars();
            Clipboard.SetText(guid);
            MessageUtil.ShowInput("已复制到剪切板", guid);
        }
    }
}

﻿using CommonUtils;
using System;
using System.Windows.Forms;

namespace CodeAssistant.Commands.IDE
{
    class GetGuidUpperNoBar
    {
        public static void Handler(object sender, EventArgs e)
        {
            var guid = StringUtil.GetGuidUpperNoBar();
            Clipboard.SetText(guid);
            MessageUtil.ShowInput("已复制到剪切板", guid);
        }
    }
}

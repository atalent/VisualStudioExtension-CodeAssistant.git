﻿using CommonUtils;
using System;

namespace CodeAssistant.Commands.Unity
{
    class AddEditorDefinition
    {
        public static void Handler(object sender, EventArgs e)
        {
            var script = EditorUtil.GetText();
            if (!script.StartsWith(UnityConstants.EditorDefinition))
                script = UnityConstants.EditorDefinition + script.Trim() + UnityConstants.DefinitionEnd;
            EditorUtil.SaveText(script);
        }
    }
}

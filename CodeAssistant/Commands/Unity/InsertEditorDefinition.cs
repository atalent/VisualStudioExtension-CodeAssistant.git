﻿using CommonUtils;
using System;

namespace CodeAssistant.Commands.Unity
{
    class InsertEditorDefinition
    {
        public static void Handler(object sender, EventArgs e)
        {
            EditorUtil.InsertLine("\r\n" + UnityConstants.EditorDefinition + UnityConstants.DefinitionEnd);
        }
    }
}


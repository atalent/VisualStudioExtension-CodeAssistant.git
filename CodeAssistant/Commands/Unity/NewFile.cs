﻿using CommonUtils;
using System;

namespace CodeAssistant.Commands.CSharp
{
    class NewFile
    {
        public static void Handler(object sender, EventArgs e)
        {
            var doc = EditorUtil.GetActiveDocument();
            if (doc == null)
            {
                MessageUtil.Show("请打开兄弟文件");
                return;
            }
            var fileName = MessageUtil.ShowInput("请输入文件名", "Class1.cs");
            var filePath = FileUtil.GetBrother(doc.FullName, fileName);
            FileUtil.SaveTo(filePath, ResourceUtil.GetText(typeof(NewFile), "Class1.txt"));
            MessageUtil.Show("文件[{}]已创建!", filePath);
        }
    }
}

﻿using CommonUtils;
using System;

namespace CodeAssistant.Commands.CSharp
{
    class NewFolder
    {
        public static void Handler(object sender, EventArgs e)
        {
            var doc = EditorUtil.GetActiveDocument();
            var folderName = MessageUtil.ShowInput("请输入文件夹名", "新建文件夹");
            var folderPath = FileUtil.GetBrother(doc.FullName, folderName);
            FolderUtil.Create(folderPath);
            MessageUtil.Show("文件夹[{}]已创建!", folderPath);
        }
    }
}

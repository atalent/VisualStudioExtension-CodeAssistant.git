﻿using CommonUtils;
using System;

namespace CodeAssistant.Commands.Unity
{
    class RemoveEditorDefinition
    {
        public static void Handler(object sender, EventArgs e)
        {
            var script = EditorUtil.GetText();
            if (script.StartsWith(UnityConstants.EditorDefinition) && script.EndsWith(UnityConstants.DefinitionEnd))
                script = script.Substring(UnityConstants.EditorDefinition.Length,
                    script.Length - UnityConstants.EditorDefinition.Length - UnityConstants.DefinitionEnd.Length);
            EditorUtil.SaveText(script);
        }
    }
}

﻿namespace CodeAssistant.Commands.Unity
{
    public static class UnityConstants
    {
        public const string EditorDefinition = "#if UNITY_EDITOR\r\n";
        public const string DefinitionEnd = "\r\n#endif";
    }
}

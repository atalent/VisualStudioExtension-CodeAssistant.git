﻿using CommonUtils;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using System;

namespace CodeAssistant.Commands.Wpf
{
    class CenterTopXmal
    {
        public static void Handler(object sender, EventArgs e)
        {
            ThreadHelper.ThrowIfNotOnUIThread();

            var dte = Package.GetGlobalService(typeof(DTE)) as DTE;
            var document = dte.ActiveDocument;

            if (document == null || FileUtil.GetExtension(document.FullName) != ".xaml")
            {
                EditorUtil.ShowMessage("Please Open and Select a Xaml File");
                return;
            }

            var text = FileUtil.GetText(document.FullName);

            var values = text.GetBetweens(" HorizontalAlignment=\"", "\"", true);
            text = text.Replace(values, " HorizontalAlignment=\"Center\"");

            values = text.GetBetweens(" VerticalAlignment=\"", "\"", true);
            text = text.Replace(values, " VerticalAlignment=\"Top\"");

            values = text.GetBetweens(" Margin=\"", "\"", true);
            text = text.Replace(values, " Margin=\"0,0,0,0\"");

            FileUtil.Save(document.FullName, text);
            EditorUtil.ShowMessage(document.Name + " has inited !");
        }
    }
}

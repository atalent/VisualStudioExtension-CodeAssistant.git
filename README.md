# VS代码助手

### 功能描述

For CSharp C#

​        Order Functions 方法排序

​        Lambda Functions 简易方法箭头化

​        Align Lambdas 箭头对齐

​        Switch equal sign Left Right 等号左右对调

For Wpf

​       Center Top Xaml 控件置顶居中

​       Center Middle Xaml 控件居中

For Unity

​      Add #if Editor Definition 添加编辑器代码标识

​	  Insert #if Editor Definition 插入编辑器代码标识

​	  Remove #if Editor Definition 移除编辑器代码标识



### 开发文档

https://docs.microsoft.com/zh-cn/visualstudio/extensibility/starting-to-develop-visual-studio-extensions?view=vs-2019


### 开发步骤

1、新建Command

2、在CodeAssistantPackage.cs和CodeAssistantPackage.vsct中注册

3、更改版本号并发布


### 开发提示

遇到报错可以尝试重启vs

提示找不到CommandID，可以修改脚本中的CommandId


### 源码

https://gitee.com/atalent/CommonUtils-dot-net.git

https://gitee.com/atalent/VisualStudioExtension-CodeAssistant.git

两个仓库放在同一文件夹


### 版本定义

.vsixmanifest

.csproj 程序集名称 程序集信息-版本


### 发布方式

**选择**

**.\bin\Debug\CodeAssistant-version.vsix**



发布

https://marketplace.visualstudio.com/manage/publishers/atalent/newvsextension

更新

https://marketplace.visualstudio.com/manage/publishers/atalent/extensions/codeassistant/edit



浏览

https://marketplace.visualstudio.com/items?itemName=atalent.CodeAssistant